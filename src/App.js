import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Seat picker newly Added after feature 2</p>
        <h1>{process.env.REACT_APP_SECRET_API_KEY}</h1>
        <h2>{process.env.REACT_APP_SECRET_API_VALUE}</h2>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
